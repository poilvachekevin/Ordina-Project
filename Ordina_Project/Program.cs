﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Ordina_Exercice
{
    class Program
    {
        //You can fin an already encrypted file into the Sample folder. (not really a .txt file but the idea is the same)
        static void Main(string[] args)
        {
            string choice;

            do
            {              
                Console.WriteLine("Which Kind of File do you want to open ? ");
                Console.WriteLine("--> 1 <-- TXT File");
                Console.WriteLine("--> 2 <-- XML File");
                Console.WriteLine("--> 3 <-- JSON File");
                choice = Console.ReadLine();
                if (choice != "1" && choice != "2" && choice !="3" && choice != "0")
                    Console.WriteLine("Please make a real choice");
                else
                {
                    if (choice == "1")
                        ConsoleManageTXT();
                    else if (choice == "2")
                        ConsoleManageXML();
                    else
                        ConsoleManageJSON();
                    
                    Console.WriteLine("Do you want to continue ? Press Enter ! Otherwise 0");
                    choice = Console.ReadLine();
                }
            } while (choice != "0");
        }

        static void ConsoleManageJSON()
        {
            Console.WriteLine("Do you want decrypt a file ? Y/N");
            if ((CheckJSON.decrypt = Console.ReadLine()) == "Y")
            {
                Console.WriteLine("Do you want to still use the following key (Y/N) ? :" + CheckJSON.key);
                if (Console.ReadLine() == "N")
                {
                    Console.WriteLine("Your new key :");
                    CheckJSON.key = Console.ReadLine();
                }
            }
            Console.WriteLine("Which is your role ? Admin = 1 and Common = 0");
            string role = Console.ReadLine();
            Console.WriteLine("Hello ! Which XML File do you want to open ? ");
            string path = Console.ReadLine();
            Console.WriteLine(CheckJSON.ReadJSON(role, path));
        }

        static void ConsoleManageXML()
        {
            Console.WriteLine("Do you want decrypt a file ? Y/N");
            if ((CheckXML.decrypt = Console.ReadLine()) == "Y")
            {
                Console.WriteLine("Do you want to still use the following key (Y/N) ? :" + CheckXML.key);
                if (Console.ReadLine() == "N")
                {
                    Console.WriteLine("Your new key :");
                    CheckXML.key = Console.ReadLine();
                }
            }
            Console.WriteLine("Which is your role ? Admin = 1 and Common = 0");
            string role = Console.ReadLine();
            Console.WriteLine("Hello ! Which XML File do you want to open ? ");
            string path = Console.ReadLine();
            Console.WriteLine(CheckXML.ReadXML(role, path));
        }

        static void ConsoleManageTXT()
        {
            Console.WriteLine("Do you want decrypt a file ? Y/N");
            if ((CheckTXT.decrypt = Console.ReadLine()) == "Y")
            {
                Console.WriteLine("Do you want to still use the following key (Y/N) ? :" + CheckTXT.key);
                if (Console.ReadLine() == "N")
                {
                    Console.WriteLine("Your new key :");
                    CheckTXT.key = Console.ReadLine();
                }
            }
            Console.WriteLine("Which is your role ? Admin = 1 and Common = 0");
            string role = Console.ReadLine();
            Console.WriteLine("Hello ! Which TXT File do you want to open ? ");
            Console.WriteLine(CheckTXT.ReadTXT(role, Console.ReadLine()));
        }
    }
}