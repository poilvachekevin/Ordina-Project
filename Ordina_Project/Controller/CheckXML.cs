﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ordina_Exercice
{
    static class CheckXML
    {
        static public string key { get; set; } = "b14ca5898a4e4133bbce2ea2315a1916";
        static public string decrypt { get; set; }

        static public string ReadXML(string role, string path)
        {
            ReadXML readXML = new ReadXML();
            List<SecurityRBAC> securityRBACs = ReadConfig.LoadProfile();
            if (securityRBACs.Any(i => i.path == (path)))
            {
                if (securityRBACs.Any(i => i.path == path && (int)i.permissions <= Int32.Parse(role)))
                {
                    if (decrypt == "N")
                        return readXML.ReturnFileValue(path);
                    else if (decrypt == "Y")
                        return EncryptionDecryptionAES.Decryption(key, readXML.ReturnFileValue(path));
                    else
                        return "Answer for decryption not understanbled !";
                }
                else
                    return "You don't have the permission !";
            }
            else
                return "not in the list of files !";
        }
    }
}
