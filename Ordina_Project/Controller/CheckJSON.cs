﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ordina_Exercice
{
    class CheckJSON
    {
        static public string key { get; set; } = "b14ca5898a4e4133bbce2ea2315a1916";
        static public string decrypt { get; set; }

        static public string ReadJSON(string path)
        {
            ReadJSON readJSONFile = new ReadJSON();
            if (decrypt == "Y")
                return readJSONFile.ReadEncryptedFile(Console.ReadLine(), key);
            else
                return readJSONFile.ReturnFileValue(Console.ReadLine());
        }

        static public string ReadJSON(string role, string path)
        {
            ReadJSON readJSONFile = new ReadJSON();
            List<SecurityRBAC> securityRBACs = ReadConfig.LoadProfile();
            if (securityRBACs.Any(i => i.path == (path)))
            {
                if (securityRBACs.Any(i => i.path == path && (int)i.permissions <= Int32.Parse(role)))
                {
                    if (decrypt == "N")
                        return readJSONFile.ReturnFileValue(path);
                    else if (decrypt == "Y")
                        return EncryptionDecryptionAES.Decryption(key, readJSONFile.ReturnFileValue(path));
                    else
                        return "Answer for decryption not understanbled !";
                }
                else
                    return "You don't have the permission !";
            }
            else
                return "not in the list of files !";
        }
    }
}
