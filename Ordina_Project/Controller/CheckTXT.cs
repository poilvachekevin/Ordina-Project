﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ordina_Exercice
{
    static class CheckTXT
    {
        static public string key { get; set; } = "b14ca5898a4e4133bbce2ea2315a1916";
        static public string decrypt { get; set; }

        static public string ReadTXT(string path)
        {
            ReadTXTFile readTXTFile = new ReadTXTFile();
            if (decrypt == "Y")
                return readTXTFile.ReadEncryptedFile(Console.ReadLine(), key);
            else
                return readTXTFile.ReturnFileValue(Console.ReadLine());
        }

        static public string ReadTXT(string role, string path)
        {
            ReadTXTFile readTXTFile = new ReadTXTFile();
            List<SecurityRBAC> securityRBACs = ReadConfig.LoadProfile();
            if (securityRBACs.Any(i => i.path == (path)))
            {
                if (securityRBACs.Any(i => i.path == path && (int)i.permissions <= Int32.Parse(role)))
                {
                    if (decrypt == "N")
                        return readTXTFile.ReturnFileValue(path);
                    else if (decrypt == "Y")
                        return EncryptionDecryptionAES.Decryption(key, readTXTFile.ReturnFileValue(path));
                    else
                        return "Answer for decryption not understanbled !";
                }
                else
                    return "You don't have the permission !";
            }
            else
                return "not in the list of files !";
        }
    }
}
