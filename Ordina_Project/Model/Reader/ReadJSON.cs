﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ordina_Exercice
{
    class ReadJSON : ReadTXTFile
    {
        public dynamic ReadParseJSON(string path)
        {
            try
            {
                JObject ElementJSON = JObject.Parse(ReturnFileValue(path));
                return ElementJSON;
            }
            catch(Newtonsoft.Json.JsonReaderException)
            {
                return "JSON not valid !";
            }
        }
    }
}
