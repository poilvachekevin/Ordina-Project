﻿using System;
using System.Xml.Linq;

namespace Ordina_Exercice
{
    class ReadXML : ReadTXTFile
    {
        public XElement ReadParseXML(string path)
        {
            try
            {
                XElement myXMLElement = XElement.Parse(ReturnFileValue(path));
                return myXMLElement;
            }
            catch (System.Xml.XmlException)
            {
                Console.WriteLine("Not an XML File !");
                return null;
            }
        }
    }
}
