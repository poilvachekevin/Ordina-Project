﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Ordina_Exercice
{
    static class EncryptionDecryptionAES
    {
        /// <summary>
        /// Simple piece of code which can make a symmetric encryption of a string. 
        /// Use an AES encryption standard. 
        /// </summary>
        /// <param name="selectedKey">156/192/256 byte key only !</param>
        /// <param name="text"></param>
        /// <returns></returns>
        internal static string Encryption(string selectedKey, string text)
        {
            byte[] array;
            byte[] vector = new byte[16];

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(selectedKey);
                aes.IV = vector;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(text);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }

        /// <summary>
        /// Simple piece of code which can make a symmetric decryption of a string. 
        /// Use an AES encryption standard. 
        /// </summary>
        /// <param name="selectedKey">156/192/256 byte key only !</param>
        /// <param name="text">Your text</param>
        /// <returns></returns>
        internal static string Decryption(string selectedKey, string text)
        {
            byte[] vector = new byte[16];
            byte[] buffer = Convert.FromBase64String(text);
            try
            {
                using (Aes crypto = Aes.Create())
                {
                    crypto.Key = Encoding.UTF8.GetBytes(selectedKey);
                    crypto.IV = vector;
                    ICryptoTransform decryptor = crypto.CreateDecryptor(crypto.Key, crypto.IV);

                    using (MemoryStream memoryStream = new MemoryStream(buffer))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch(Exception)
            {
                return "Decryption failed !";
            }
        }
    }
}
