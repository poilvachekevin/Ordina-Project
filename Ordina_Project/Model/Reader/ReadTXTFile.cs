﻿using System;
using System.IO;

namespace Ordina_Exercice
{
    class ReadTXTFile
    {
        public string ReadEncryptedFile(string path, string key)
        {
            return EncryptionDecryptionAES.Decryption(key, ReturnFileValue(path));
        }

        public string ReturnFileValue(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    string text = "";
                    using (StreamReader stream = new StreamReader(path))
                    {
                        int ch;
                        while ((ch = stream.Read()) != -1)
                            if (IsException(ch))
                                return "Binary File !";
                            else text += (char)ch;
                    }
                    return text;
                }
                else return "Not a Valid Path !";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        private static bool IsException(int ch)
        {
            if ((ch > 0 && ch < 8) || (ch > 13 && ch < 26))
                return true;
            else return false;
        }
    }
}
