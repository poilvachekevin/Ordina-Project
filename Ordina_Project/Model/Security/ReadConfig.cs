﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace Ordina_Exercice
{
    static class ReadConfig
    {
        static public List<SecurityRBAC> LoadProfile()
        {
            List<SecurityRBAC> listSecurityRBAC = new List<SecurityRBAC>();
            ReadXML readXML = new ReadXML();

            try
            {
                XElement config = readXML.ReadParseXML("..\\..\\..\\Samples\\Roles\\RolesConfig.xml");
                var listRole = config.Elements("permission");
                foreach (var item in listRole)
                {
                    SecurityRBAC securityRBAC = new SecurityRBAC();
                    securityRBAC.permissions = (SecurityRBAC.PermissionsAccess)Int32.Parse(item.Element("permissionAccess").Value);
                    securityRBAC.path = Path.GetFullPath(item.Element("path").Value);
                    listSecurityRBAC.Add(securityRBAC);
                }
                return listSecurityRBAC;
            }
            catch(Exception)
            {
                Console.WriteLine("Config file not correct !");
                return null;
            }

        }
    }
}
