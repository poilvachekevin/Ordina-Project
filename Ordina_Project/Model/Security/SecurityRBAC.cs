﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ordina_Exercice
{
    public class SecurityRBAC
    {
        public PermissionsAccess permissions { get; set; }
        public string path { get; set; }

        public enum PermissionsAccess
        {
            Common = 0,
            Admin = 1
        }
    } 
}
